/**
 * @Author: Indhu Ramalingam
 * @Name: STS_ClsTriggerOpportunityHelper
 * @CreatedDate: April 2018
 * @Description: Helper class for ClsTriggerOpportunityHandler
 * @Release: STS PR2.0 
 **/
public class STS_ClsTriggerOpportunityHelper {
    
    private final static String STR_SYSTEM = 'System';
    
    /**
     * @method: probabilityCheckForExternalUsers
     * @created date: April 2018
     * @description: User manual adjustment of probability that is still in the range of new level of probability should be retained. 
     *                 It should be overwritten ONLY when it's NOT within new level of range/last modified by System;
     *            Method is being called in after trigger only for Portal Users as the fields SPP, CP are auto populated using trigger
     *   and the background formula fields are calculated only after the values are saved.
     * @param: ClsWrappers.TriggerContext trgCtx
     * @return: null
     * @release: Sts PR2.0 
     **/
    public static void probabilityCheckForExternalUsers(ClsWrappers.TriggerContext trgCtx) {
        Set<Id> updatedRecords = new Set<Id> ();
        if(trgCtx.isUpdate){
            List<String> fieldAPINames = new List<String> {'Customer_Buying_Path__c', 'StageName',
                                                            'Sales_Pursuit_Progress__c'};
            updatedRecords = ClsTriggerFactory.fieldUpdateCheck(trgCtx, fieldAPINames);
        }
        
        
        Map<String, STS_ProbabilityRange__c> mapRanges = STS_ProbabilityRange__c.getAll();      //Get values from Custom Settings
        List<String> percentRange = new List<String>(); 
        Opportunity oldOpp;
        Opportunity triggerNewOpp;
        Boolean isABBProb1NotNull;
        Boolean isABBProb2NotNull;

        Boolean isValueChanged = false;
        Id rbRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Recurring_Opportunity).getRecordTypeId();
        List<Opportunity> lstOpp = new List<Opportunity>();
        Opportunity newOpp = new Opportunity();

        for (sobject so : trgCtx.newList) {            
            percentRange = new List<String>(); 
            triggerNewOpp = (Opportunity) so;
            newOpp = new Opportunity(Id = triggerNewOpp.Id, StageName = triggerNewOpp.StageName, Customer_Buying_Path__c = triggerNewOpp.Customer_Buying_Path__c,
           Sales_Pursuit_Progress__c = triggerNewOpp.Sales_Pursuit_Progress__c, 
ABB_Probability__c = triggerNewOpp.ABB_Probability__c, ProbValue_ModifiedBy_Technical__c = triggerNewOpp.ProbValue_ModifiedBy_Technical__c);

            
            isABBProb1NotNull = triggerNewOpp.ABB_Background_Probability1__c != null ? true : false;           //Set boolean whether ABB_Background_Probability1__c is null or not null
            isABBProb2NotNull = triggerNewOpp.ABB_Background_Probability2__c != null ? true : false;           //Set boolean whether ABB_Background_Probability2__c is null or not null
            
            if(trgCtx.oldMap != null){
                oldOpp = (cases)
            }                   


            if(trgCtx.isInsert || (trgCtx.isUpdate && updatedRecords.contains(newOpp.id))){
            if(trgCtx.isUpdate) {
                isValueChanged = newOpp.Customer_Buying_Path__c != oldOpp.Customer_Buying_Path__c || newOpp.StageName != oldOpp.StageName
                                    || newOpp.Sales_Pursuit_Progress__c != oldOpp.Sales_Pursuit_Progress__c;        
            }

            if(newOpp.RecordTypeId != rbRecordTypeId){
            
                //Moved the below logic from the Workflow 'Opportunity Probability Range Baseline'
                if(trgCtx.isInsert || isValueChanged) {          

                    if(isABBProb1NotNull) { 
                        newOpp.ABB_Probability_for_Range__c = triggerNewOpp.ABB_Background_Probability1__c; 
                    }
                    else if(isABBProb2NotNull) { 
                        newOpp.ABB_Probability_for_Range__c = triggerNewOpp.ABB_Background_Probability2__c; 
                    }
                  
                }
                
                //Moved the below logic from the Workflow 'Opportunity Probability Update'
                if(mapRanges.containsKey(String.valueOf(newOpp.ABB_Probability_for_Range__c)) 
                    && (trgCtx.isInsert || (newOpp.ABB_Probability__c == oldOpp.ABB_Probability__c && isValueChanged))) {                    
                    
                    percentRange = (mapRanges.get(String.valueOf(newOpp.ABB_Probability_for_Range__c)).Probability_Range__c).split(Label.Sys_Comma);

                    //Extra if condition added here to check whether the User entered Probability is within new level of Range
                    if(!percentRange.contains(String.valueOf(newOpp.ABB_Probability__c))
                            || (!String.isBlank(newOpp.ProbValue_ModifiedBy_Technical__c) && (STR_SYSTEM).equals(newOpp.ProbValue_ModifiedBy_Technical__c))) {
                        if(isABBProb1NotNull) {
                            newOpp.ABB_Probability__c = triggerNewOpp.ABB_Background_Probability1__c;
                        } 
                        else if(isABBProb2NotNull) { 
                            newOpp.ABB_Probability__c = triggerNewOpp.ABB_Background_Probability2__c;
                        }
                    }
                    else { 
                        lstOpp.add(newOpp);
                        break; 
                    }                                         
                }          
            }
            }
                
                if(newOpp.RecordTypeId != rbRecordTypeId){
                //Check whether the Probability value is set by User or System
                if(trgCtx.isInsert || (newOpp.ABB_Probability__c != oldOpp.ABB_Probability__c &&
                    ((isABBProb1NotNull && triggerNewOpp.ABB_Background_Probability1__c == oldOpp.ABB_Background_Probability1__c) ||
                    (isABBProb2NotNull && triggerNewOpp.ABB_Background_Probability2__c == oldOpp.ABB_Background_Probability2__c)))) {
                    newOpp.ProbValue_ModifiedBy_Technical__c = Label.ClsTriggerCaseHandler_User;                                     //'User'
                }
                else if(trgCtx.isInsert || (newOpp.ABB_Probability__c != oldOpp.ABB_Probability__c &&
                    ((isABBProb1NotNull && triggerNewOpp.ABB_Background_Probability1__c != oldOpp.ABB_Background_Probability1__c) ||
                    (isABBProb2NotNull && triggerNewOpp.ABB_Background_Probability2__c != oldOpp.ABB_Background_Probability2__c)))) {

                    if(triggerNewOpp.ABB_Background_Probability1__c == newOpp.ABB_Probability__c || triggerNewOpp.ABB_Background_Probability2__c == newOpp.ABB_Probability__c) {
                        newOpp.ProbValue_ModifiedBy_Technical__c = STR_SYSTEM; 
                    }
                }
            }
            if(trgCtx.isInsert || isValueChanged) {                                    
                lstOpp.add(newOpp);
            }
        }
        //STS PR3.0 UAT Defect - 6252: To update the SetupOwnerId in Custom Setting if already present
        String changesSet = '';
        Boolean updateCheckException = false;
        changesSet = DisbleValidationDAO.addDisableValidation();  
        try {
            //Perform DML in After Trigger by bypassing recursive trigger execution
            if(lstOpp != null && lstOpp.size() > 0) {
                ClsAccountUtil.isAccMergeFlag = true;
                ClsTriggerFactory.isSkipOpptyTrigger = true;   
                Database.upsert(lstOpp);
            }
        }
        catch(Exception ex) {
            updateCheckException = true;
            if(String.isNotEmpty(changesSet)){
                DisbleValidationDAO.removeDisableValidation(changesSet,updateCheckException);
            }
            
            Throw new LogDAO.LogDAOException('Error Message: '+ex.getMessage());
            
        }
        if(String.isNotEmpty(changesSet)){
            DisbleValidationDAO.removeDisableValidation(changesSet,updateCheckException);
        }
    }

/**   
    * @Author: Indhu Ramalingam
    * @name: probabilityRangeCheckForCQP
    * @CreateDate: May 2018
    * @release: Sts PR2.0 Hypercare (CQP sync issue) - Opportunity Management (Req - 5690; User Story: 1336) 
    * @Description: User manual adjustment of probability that is still in the range of new level of probability should be retained. 
    *                 It should be overwritten ONLY when it's NOT within new level of range/last modified by System.
    * Only for Opportunity Update
    **/
    public static void probabilityRangeCheckForCQP(List<Opportunity> newOppList, Map<Id, Opportunity> oldOppMap) {
    
        Map<String, STS_ProbabilityRange__c> mapRanges = STS_ProbabilityRange__c.getAll();      //Get values from Custom Settings
        List<String> percentRange = new List<String>(); 
        Opportunity oldOpp = new Opportunity();
        Boolean isABBProb1NotNull;
        Boolean isABBProb2NotNull;
        
        Boolean isValueChanged;
        Id rbRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(Label.Recurring_Opportunity).getRecordTypeId();
        
        for (Opportunity newOpp : newOppList) {            
            percentRange = new List<String>(); 
        
            if(oldOppMap != null){
                oldOpp = oldOppMap.get(newOpp.id);
            }                   
            isABBProb1NotNull = newOpp.ABB_Background_Probability1__c != null ? true : false;           //Set boolean whether ABB_Background_Probability1__c is null or not null
            isABBProb2NotNull = newOpp.ABB_Background_Probability2__c != null ? true : false;           //Set boolean whether ABB_Background_Probability2__c is null or not null
             
isValueChanged = newOpp.Customer_Buying_Path__c != oldOpp.Customer_Buying_Path__c || newOpp.StageName != oldOpp.StageName
  || newOpp.Sales_Pursuit_Progress__c != oldOpp.Sales_Pursuit_Progress__c;                                  
            
            if(newOpp.RecordTypeId != rbRecordTypeId){
                //Moved the below logic from the Workflow 'Opportunity Probability Range Baseline'
                if(isValueChanged) {                 
                    
                    if(isABBProb1NotNull) { 
                        newOpp.ABB_Probability_for_Range__c = newOpp.ABB_Background_Probability1__c; 
                    }
                    else if(isABBProb2NotNull) { 
                        newOpp.ABB_Probability_for_Range__c = newOpp.ABB_Background_Probability2__c; 
                    }
                    else { 
                        newOpp.ABB_Probability_for_Range__c = 0; 
                    }
                }
                
                //Moved the below logic from the Workflow 'Opportunity Probability Update'
                if(mapRanges.containsKey(String.valueOf(newOpp.ABB_Probability_for_Range__c)) 
                    && newOpp.ABB_Probability__c == oldOpp.ABB_Probability__c && isValueChanged) {                    
                                                
                    percentRange = (mapRanges.get(String.valueOf(newOpp.ABB_Probability_for_Range__c)).Probability_Range__c).split(Label.Sys_Comma);
                    
                    //Extra if condition added here to check whether the User entered Probability is within new level of Range
                    if(!percentRange.contains(String.valueOf(newOpp.ABB_Probability__c))
                                || (!String.isBlank(newOpp.ProbValue_ModifiedBy_Technical__c) && (STR_SYSTEM).equals(newOpp.ProbValue_ModifiedBy_Technical__c))) {
                        if(isABBProb1NotNull) {
                            newOpp.ABB_Probability__c = newOpp.ABB_Background_Probability1__c;
                        } 
                        else if(isABBProb2NotNull) { 
                            newOpp.ABB_Probability__c = newOpp.ABB_Background_Probability2__c;
                        }
                    }
                    else { break; }                                         
                }          
                
                //Check whether the Probability value is set by User or System
                if(newOpp.ABB_Probability__c != oldOpp.ABB_Probability__c &&
 ((isABBProb1NotNull && newOpp.ABB_Background_Probability1__c == oldOpp.ABB_Background_Probability1__c) ||
  (isABBProb2NotNull && newOpp.ABB_Background_Probability2__c == oldOpp.ABB_Background_Probability2__c))) {
                    newOpp.ProbValue_ModifiedBy_Technical__c = Label.ClsTriggerCaseHandler_User;                                     //'User'
                }
                else if(newOpp.ABB_Probability__c != oldOpp.ABB_Probability__c &&
((isABBProb1NotNull && newOpp.ABB_Background_Probability1__c != oldOpp.ABB_Background_Probability1__c) ||
   (isABBProb2NotNull && newOpp.ABB_Background_Probability2__c != oldOpp.ABB_Background_Probability2__c))) {
                    
                    if(newOpp.ABB_Background_Probability1__c == newOpp.ABB_Probability__c || newOpp.ABB_Background_Probability2__c == newOpp.ABB_Probability__c) {
                        newOpp.ProbValue_ModifiedBy_Technical__c = STR_SYSTEM; 
                    }
                }
            }
        }
    }
    
    /**   
    * @Description:  This method retrieves all currency iso codes and related currency conversion rates.
    *  Then it calculates the opportunities USD amount and saves it into technical field "Technical_Amount_USD__c".
    * @Release R3
    * @author  Hari Charan (Accenture)
    * @created  2018-25-09
    */   
    public static void opportunityTechnicalAmountUSDUpdate(List<Opportunity> sObjList){
        for(Opportunity oppy : sObjList){
            if(oppy.Opportunity_Value__c != null){
                oppy.Technical_Amount_USD__c =  ClsUtil.transformIsoCode(oppy.Opportunity_Value__c, oppy.CurrencyIsoCode, 'USD').setScale(2);
            }    
        }
    }
    
    /*
     *Author: Piotr Wyszynski (Accenture)
 *  Description: This is temporary fix for master data related issues on opportunity picklists
 */
    
    
    // Defect 8707.
    
    public static void ProbablityUpdate (List<Opportunity> OppTemp){
        for(Opportunity OppVal : OppTemp){
            if(OppVal.Probability!=null || OppVal.Probability ==null ){
                System.debug('Custom Field values are ======'+OppVal.ABB_Probability__c);
                System.debug('Standard Field values are ======'+OppVal.Probability);
                OppVal.Probability = OppVal.ABB_Probability__c;
            }
        }
    } 
   //  START SFDC-JULY2020-06-CR TQ RFQ Received Date TODAY Opportunity   
    public static void autoPopulateRFQdate (List<Opportunity> newOpportunities){
         for(Opportunity newOppy : newOpportunities){
            if((newOppy.StageName=='Bidding'||newOppy.StageName=='Negotiation') && newOppy.RFQ_Issue_Date__c==null){
               newOppy.RFQ_Issue_Date__c = System.today();               
            }
        }       
    } 
    // END SFDC-JULY2020-06-CR TQ RFQ Received Date TODAY Opportunity
    
     private static Map<String, PicklistInformation> preparePicklistMapping() {
         Map<String, PicklistInformation> picklistMapping = new Map<String, PicklistInformation>();
         picklistMapping.put('BU__c', new PicklistInformation('ApplicationClassDomain__c', 4));
         picklistMapping.put('Application__c', new PicklistInformation('ApplicationClass__c', 6));
         picklistMapping.put('ABB_Domain__c', new PicklistInformation('BusinessLineDomain__c', 4));
         picklistMapping.put('Business_Line__c', new PicklistInformation('BusinessLine__c', 6));
         picklistMapping.put('ABB_Industry_Usage_Level1__c', new PicklistInformation('IndustryUsageLevel1__c', 3));
         picklistMapping.put('ABB_Industry_Usage_Level2__c', new PicklistInformation('IndustryUsageLevel2__c', 6));
         picklistMapping.put('ABB_Industry_Usage_Level3__c', new PicklistInformation('IndustryUsageLevel3__c', 9));
         picklistMapping.put('Channel_Class_Level1__c', new PicklistInformation('ChannelClassLevel1__c', 6));
         picklistMapping.put('Channel_Class_Level2__c', new PicklistInformation('ChannelClassLevel2__c', 6));
         picklistMapping.put('Service_Category__c', new PicklistInformation('ServiceCategoryLevel1__c', 3));
         return picklistMapping;
     }
    
    public static void fixPicklistValues(List<Opportunity> newOpportunities) {
           PicklistMappings mappings = new PicklistMappings();
            for (Opportunity opp : newOpportunities) {
                for (String picklistName : mappings.picklistInformations.keySet()) {
                    mapPicklist(opp, picklistName, mappings);
                }
            }
    }
   
    private static void mapPicklist(Opportunity opp, String picklistName, PicklistMappings mappings) {
        String fieldValue = (String)opp.get(picklistName);
        fixServiceCategory(opp, picklistName, mappings.oldServiceCategory);
        
        if (!String.isEmpty(fieldValue) && fieldValue.length() > mappings.picklistInformations.get(picklistName).allowedLength) {
            String correctedValue = 
                PicklistManager.getPicklistValue(
                    'MasterdataCodeToNameMapping__c', 
                    mappings.picklistInformations.get(picklistName).metadataFieldName, 
                    fieldValue
                );
            if (correctedValue != '') {
                opp.put(picklistName, correctedValue);
            }
        }
    }
    
    private static void fixServiceCategory(Opportunity opp, String picklistName, Map<String, String> newByOldServiceCategory) {
        if (picklistName == 'Service_Category__c' && 
            opp.Service_Category__c != null && 
            newByOldServiceCategory.keySet().contains(opp.Service_Category__c)) {
            opp.Service_Category__c = newByOldServiceCategory.get(opp.Service_Category__c);
        }
    }
    
    private static Map<String, String> getMappingForOldServiceCategory() {
        List<Schema.PicklistEntry> picklistEntries = 
            MasterdataCodeToNameMapping__c.ServiceCategoryLevel1__c.getDescribe().getPicklistValues();
        
        Map<String, String> mapping = new Map<String, String>();
        for (Schema.PicklistEntry pe : picklistEntries) {
            List<String> labelTokens = pe.getLabel().split('-'); //labels have format: CODE - NAME,
            mapping.put(labelTokens.get(1).trim(), labelTokens.get(0).trim());
        }
        return mapping;
    }
public static void InsertCampInf(ClsWrappers.TriggerContext trgCtx,List<Opportunity> sObjList){
        List<CampaignInfluence> listCI = new List<CampaignInfluence>();     
        Set<Id> updatedRecords = new Set<Id> ();
        List<Opportunity>Newoppwithcamp = new List<Opportunity> ();
        CampaignInfluenceModel campInfMod = [select Id from CampaignInfluenceModel where MasterLabel = :label.CAMPAIGN_MODEL_LABEL LIMIT 1];
        for(Opportunity oppy : sObjList){
            if(oppy.CampaignId != null){
                Newoppwithcamp.add(oppy);
            }
        }
         if(trgCtx.isUpdate){
            List<String> fieldAPINames = new List<String> {'CampaignId'};
            updatedRecords = ClsTriggerFactory.fieldUpdateCheck(trgCtx, fieldAPINames);
            if(!updatedRecords.isEmpty() && !Newoppwithcamp.isEmpty())
            {
            for(Opportunity oppy : Newoppwithcamp){
                CampaignInfluence newCI = new CampaignInfluence();
                newCI.campaignId = oppy.CampaignId;
                newCI.opportunityId = oppy.Id; 
                newCI.modelId = campInfMod.Id;
                listCI.add(newCI); 
            }
        }
    }
    else if(trgCtx.isInsert){
        if(!Newoppwithcamp.isEmpty()){
            for (Opportunity op : Newoppwithcamp){
                CampaignInfluence newCI = new CampaignInfluence();
                newCI.campaignId = op.CampaignId;
                newCI.opportunityId = op.Id; 
                newCI.modelId = campInfMod.Id;
                listCI.add(newCI);
            }
        }
    }
    if(!listCI.isEmpty()){
    Database.insert(listCI,false);
    }
     }
     
     private class PicklistInformation {
         public String metadataFieldName;
         public Integer allowedLength;
         public PicklistInformation(String fieldName, Integer length) {
             metadataFieldName = fieldName;
             allowedLength = breadth;
         }
     }
	 
	 
	 
	 
    
    private class PicklistMappings {
        public Map<String, PicklistInformation> picklistInformations = preparePicklistMapping(); 
        public Map<String, String> oldServiceCategory = getMappingForOldServiceCategory();
    }
}